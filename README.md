# goto-ru/chain/blockchain

Code powering the blockchain nodes (as opposed to clients).

## Building

```sh
# install mozilla nix overlay https://github.com/mozilla/nixpkgs-mozilla
nix-shell
cargo build
```
