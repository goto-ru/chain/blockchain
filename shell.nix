with import <nixpkgs> {};

stdenv.mkDerivation {
	name = "goto-ru.chain.blockchain";

	buildInputs = with pkgs; [ (rustChannelOf { date = "2017-09-17"; channel = "nightly"; }).rust pkgconfig libsodium ];
}
